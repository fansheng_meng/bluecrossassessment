import java.util.Scanner;

public class Application {

	public static void main(String[] args) {
		OperateFan();
	}
	
	public static void OperateFan(){
		System.out.println(">> Are you aready to operate the Fan? (Yes or No)");
		Scanner scan = new Scanner(System.in);
		String answer = scan.nextLine();
		if(!answer.toUpperCase().equals("YES")){
			System.out.println("You said no, or your answer is invalid.");
			return;
		}
		Fan fan = Fan.getFan();
		System.out.println(">> There are two pull cords: A and B. A controls speed, B controls direction. "
				+ "Please give your choice. If you wan to quit press C.");
		while(true){
			answer = scan.nextLine();
			try {
				if(answer.toUpperCase().equals("A")){
					fan.speedUp();
					System.out.println(fan.toString());
				}else if(answer.toUpperCase().equals("B")){
					fan.changeDirection();
					System.out.println(fan.toString());
				}else if(answer.toUpperCase().equals("C")){
					System.out.println("Goodbye");
					return;
				}else{
					System.out.println(">> Your answer is invalid. Please choose A, B, C");
				}
			} catch (Exception e) {
				System.out.println(">> Your answer is invalid. Please choose A, B, C");
			}
			System.out.println(">> Please choose A, B, C");
		}
	}

}
