public class Fan {
	private static Fan fan;
	private static int speed = 0;
	private Enum direction = Direction.Clockwise;
	private Fan(){};
	public static Fan getFan(){
		fan = new Fan();
		return fan;
	}
	public int getSpeed() {
		return speed;
	}
	public void speedUp() {
		switch (speed) {
		case 0: speed =1; break;
		case 1: speed =2; break;
		case 2: speed =3; break;
		case 3: speed =0; break;
		default:
			break;
		}
	}
	public Enum getDirection() {
		return direction;
	}
	public void changeDirection() {
		if(direction == Direction.Clockwise) direction=Direction.Counterclockwise;
		else direction=Direction.Clockwise;
	}
	@Override
	public String toString() {
		return "Fan : [speed =" + speed + ", direction =" + direction + "]";
	}
	
}
